var x = 10;
if (true) {
    var x = 20; // variabel x diubah di dalam blok
}
console.log(x); // Output: 20

let y = 10;
if (true) {
    let y = 20; // variabel y hanya terlihat di dalam blok
}
console.log(y); // Output: 10

const z = 10;
if (true) {
    // Anda tidak dapat mengubah z setelah deklarasi
    // z = 20; // Ini akan menghasilkan kesalahan
}
console.log(z); // Output: 10
